const minLengthValidator = (value, minLength) => {
    return value.length >= minLength;
};

const requiredValidator = value => {
    return value.trim() !== '';
};

const genderValidator = value => {
const re = /^MALE$|^FEMALE$|^NA$/;
    return re.test(String(value).toUpperCase());
};

const requiredValidatorDate = value => {
    const re = /^(19|20)\d\d[-](0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])/;
    return re.test(String(value));
}

const requiredUserFromat = value => {
    const re = /^[a-zA-Z0-9._]{6,20}/;
    return re.test(String(value));
}

const requiredLimits = value => {
    if( value > 0 && value <= 700 )
        return true;
    else return false;
}

const validate = (value, rules) => {
    let isValid = true;

    for (let rule in rules) {

        switch (rule) {
            case 'minLength': isValid = isValid && minLengthValidator(value, rules[rule]);
                break;

            case 'isRequired': isValid = isValid && requiredValidator(value);
                break;

            case 'genderValid': isValid = isValid && genderValidator(value);
                break;

            case 'dateValid': isValid = isValid && requiredValidatorDate(value);
                break;
            case 'format': isValid = isValid && requiredUserFromat(value);
                break;

            case 'dossageLimit': isValid = isValid && requiredLimits(value);
                break;

            default: isValid = true;
        }

    }

    return isValid;
};

export default validate;