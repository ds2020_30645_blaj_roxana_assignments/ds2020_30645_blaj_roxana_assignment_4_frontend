import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    users: '/userr',
    patients:'/patient',
    caregivers:"/caregiver",
    medication: "/medication",
    name:"/name/",
    username: "/username/",
    patientCaregiv: "/patientCaregiver",
    updateMedicalRecord:"/updateMedicalRecord/"
};

function getUsers(callback) {
    let request = new Request(HOST.backend_api + endpoint.users, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patients, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getCaregivers(callback) {
    let request = new Request(HOST.backend_api + endpoint.caregivers, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getMedication(callback) {
    let request = new Request(HOST.backend_api + endpoint.medication, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postPatient(userPacient, callback){
    let request = new Request(HOST.backend_api + endpoint.users + endpoint.patients , {
        method: 'POST',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(userPacient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function postCaregiver(userCaregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.users + endpoint.caregivers , {
        method: 'POST',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(userCaregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteUser(username, callback){
    let request = new Request(HOST.backend_api + endpoint.users + endpoint.username + username , {
        method: 'DELETE',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function updatePatient(name, patient, callback){
    let request = new Request(HOST.backend_api + endpoint.patients + "/" + name, {
        method: 'PUT',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateCaregiver(name, caregiver, callback){
    let request = new Request(HOST.backend_api + endpoint.caregivers + "/" + name, {
        method: 'PUT',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(caregiver)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function postMedication( medication, callback){
    let request = new Request(HOST.backend_api + endpoint.medication , {
        method: 'POST',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deleteMedication(name, callback){
    let request = new Request(HOST.backend_api + endpoint.medication + endpoint.name + name , {
        method: 'DELETE',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log("URL: " + request.url);
    RestApiClient.performRequest(request, callback);
}

function updateMedication(name, medication, callback){
    let request = new Request(HOST.backend_api + endpoint.medication + endpoint.name + name, {
        method: 'PUT',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medication)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function updateMedicalRecord(name, medicalRecord, callback){
    let request = new Request(HOST.backend_api + endpoint.patients + endpoint.updateMedicalRecord + name, {
        method: 'PUT',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(medicalRecord)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function getPatientCaregiver(callback){
    let request = new Request(HOST.backend_api + endpoint.users + endpoint.patientCaregiv, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export{
    getUsers,
    getPatients,
    getCaregivers,
    getMedication,
    getPatientCaregiver,
    postPatient,
    postCaregiver,
    deleteUser,
    updatePatient,
    updateCaregiver,
    updateMedicalRecord,
    postMedication,
    deleteMedication,
    updateMedication
};
