import React from 'react';

import {
    Card,
    Row,
    Col,
    Button,
    Modal,
    ModalHeader,
    ModalBody

} from 'reactstrap'
import { withRouter } from "react-router-dom";

import * as API_USERS from "../doctor/api/doctor-api";

import UserTable from "./components/user-table";
import PatientTable from "./components/patient-table";
import CaregiverTable from "./components/caregiver-table";
import PatientAddForm from "./components/patient-add-form";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import MedicationTable from "./components/medication-table";
import CaregiverAddForm from "./components/caregiver-add-form";
import UserDeleteForm from "./components/user-delete-form";
import MedicationDeleteForm from "./components/medication-delete-form";
import MedicationAddForm from "./components/medication-add-form";
import MedicationUpdateForm from "./components/medication-update-form";
import PatientUpdateForm from "./components/patient-update-form";
import PatientUpdateMedicalRecForm from "./components/patient-update-medicalrec-form";
import CaregiverPatientTable from "./components/caregiver-patient-table";
import CaregiverUpdateForm from "./components/caregiver-update-form";

class DoctorContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleFormAddP = this.toggleFormAddP.bind(this);
        this.toggleFormAddC=this.toggleFormAddC.bind(this);
        this.toggleFormDelete = this.toggleFormDelete.bind(this);
        this.toggleFormDeleteM = this.toggleFormDeleteM.bind(this);
        this.toggleFormAddM = this.toggleFormAddM.bind(this);
        this.toggleFormUpdateM = this.toggleFormUpdateM.bind(this);
        this.toggleFormUpdateP = this.toggleFormUpdateP.bind(this);
        this.toggleFormUpdateUMR =this.toggleFormUpdateUMR.bind(this);
        this.toggleFormUpdateC = this.toggleFormUpdateC.bind(this);
        this.reload = this.reload.bind(this);
        this.reloadP = this.reloadP.bind(this);
        this.reloadC=this.reloadC.bind(this);
        this.reloadM=this.reloadM.bind(this);
        this.reloadAM = this.reloadAM.bind(this);
        this.reloadUM = this.reloadUM.bind(this);
        this.reloadUP = this.reloadUP.bind(this);
        this.reloadUMR = this.reloadUMR.bind(this);
        this.reloadUC = this.reloadUC.bind(this);

        this.state = {
            selectedAP: false,
            selectedAC: false,
            selectedDU: false,
            selectedAM: false,
            selectedUM: false,
            selectedM: false,
            selectedUP: false,
            selectedUMR: false,
            selectedUC: false,
            collapseForm: false,
            tableData: [],
            isLoadedVU: false,
            isLoadedVP: false,
            isLoadedVC: false,
            isLoadedM: false,
            isLoadedCP: false,
            errorStatus: 0,
            error: null
        };


    }

    componentDidMount() {

        this.fetchUsers();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchMedication();
        this.fetchCaregiverWithPatients();
    }


    toggleFormAddP() {
        this.setState({selectedAP: !this.state.selectedAP});
    }

    reloadP() {
        this.setState({
            isLoadedVU: false,
            isLoadedVP: false,
            isLoadedCP: false,
            isOpen:false
        });
        this.toggleFormAddP();
        this.fetchUsers();
        this.fetchPatients();
        this.fetchCaregiverWithPatients()
    }

    toggleFormAddC() {
        this.setState({selectedAC: !this.state.selectedAC});
    }

    reloadC() {
        this.setState({
            isLoadedVU: false,
            isLoadedVC:false,
            isOpen:false
        });
        this.toggleFormAddC();
        this.fetchUsers();
        this.fetchCaregivers();
    }

    toggleFormDelete(){
        this.setState({selectedDU: !this.state.selectedDU});
    }

    reload(){
        this.setState({
            isLoadedVU: false,
            isLoadedVC: false,
            isLoadedVP: false,
            isLoadedCP: false,
            isOpen: false
        });
        this.toggleFormDelete();
        this.fetchUsers();
        this.fetchPatients();
        this.fetchCaregivers();
        this.fetchCaregiverWithPatients()
    }

    toggleFormDeleteM(){
        this.setState({selectedM: !this.state.selectedM});
    }

    reloadM(){
        this.setState({
            isLoadedM:false,
            isOpen: false
        });
        this.toggleFormDeleteM();
        this.fetchMedication();
    }

    toggleFormAddM(){
        this.setState({selectedAM: !this.state.selectedAM});
    }

    reloadAM(){
        this.setState({
            isLoadedM:false,
            isOpen: false
        });
        this.toggleFormAddM();
        this.fetchMedication();
    }

    toggleFormUpdateM(){
        this.setState({selectedUM: !this.state.selectedUM});
    }

    reloadUM(){
        this.setState({
            isLoadedM:false,
            isOpen: false
        });
        this.toggleFormUpdateM();
        this.fetchMedication();
    }

    toggleFormUpdateP(){
        this.setState({selectedUP: !this.state.selectedUP});
    }

    reloadUP(){
        this.setState({
            isLoadedVP:false,
            isLoadedCP: false,
            isOpen: false
        });
        this.toggleFormUpdateP();
        this.fetchPatients();
        this.fetchCaregiverWithPatients()
    }

    toggleFormUpdateUMR(){
        this.setState({selectedUMR: !this.state.selectedUMR});
    }

    reloadUMR(){
        this.setState({
            isLoadedVP:false,
            isOpen: false
        });
        this.toggleFormUpdateUMR();
        this.fetchPatients();
    }

    toggleFormUpdateC(){
        this.setState({selectedUC: !this.state.selectedUC});
    }

    reloadUC(){
        this.setState({
            isLoadedVC: false,
            isLoadedCP:false,
            isOpen: false
        });
        this.toggleFormUpdateC();
        this.fetchCaregivers();
        this.fetchCaregiverWithPatients()
    }



    fetchUsers() {
        return API_USERS.getUsers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoadedVU: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchPatients() {
        return API_USERS.getPatients((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoadedVP: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchCaregivers() {
        return API_USERS.getCaregivers((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoadedVC: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchMedication() {
        return API_USERS.getMedication((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoadedM: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    fetchCaregiverWithPatients(){
        return API_USERS.getPatientCaregiver((result, status, err) => {

            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoadedCP: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }



    render() {
        const handleLogout = () => {

            localStorage.clear();
            this.props.history.push("/login")
        };

        const loggedInAsDoctor =localStorage.getItem("role");
        if (loggedInAsDoctor==="DOCTOR") {
            return (

                <div>

                    <div className="bg-light clearfix">
                        <button type="button" className="btn  bg-secondary float-right mr-2 mt-1"
                                onClick={handleLogout}>
                            Logout
                        </button>
                        <span><h2> Doctor </h2></span>
                    </div>

                    <Card>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>

                                <Button color="primary" onClick={this.toggleFormAddP}>Add Patient </Button>
                                &nbsp;&nbsp;&nbsp;
                                <Button color="primary" onClick={this.toggleFormAddC}>Add Caregiver </Button>
                                &nbsp;&nbsp;&nbsp;
                                <Button color="primary" onClick={this.toggleFormDelete}>Delete User </Button>
                                &nbsp;&nbsp;&nbsp;
                                <Button color="primary" onClick={this.toggleFormUpdateP}>Update Patient </Button>
                                &nbsp;&nbsp;&nbsp;
                                <Button color="primary" onClick={this.toggleFormUpdateC}>Update Caregiver </Button>
                                &nbsp;&nbsp;&nbsp;
                                <Button color="primary" onClick={this.toggleFormUpdateUMR}>Update medical
                                    record(pacient) </Button>

                            </Col>
                        </Row>
                        <br/>
                        <Row> <Col sm={{size: '8', offset: 1}}>

                            <Button color="primary" onClick={this.toggleFormAddM}>Add Medication </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormDeleteM}>Delete Medication </Button>
                            &nbsp;&nbsp;&nbsp;
                            <Button color="primary" onClick={this.toggleFormUpdateM}>Update Medication </Button>

                        </Col>
                        </Row>
                        <br/>

                        <h3 style={{marginLeft: 60}}> View users </h3>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>
                                {this.state.isLoadedVU && <UserTable tableData={this.state.tableData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />}
                            </Col>
                        </Row>

                        <br/>
                        <h3 style={{marginLeft: 60}}> View patients </h3>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>
                                {this.state.isLoadedVP && <PatientTable tableData={this.state.tableData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />}
                            </Col>
                        </Row>

                        <br/>
                        <h3 style={{marginLeft: 60}}> View caregivers </h3>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>
                                {this.state.isLoadedVC && <CaregiverTable tableData={this.state.tableData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />}
                            </Col>
                        </Row>

                        <br/>
                        <h3 style={{marginLeft: 60}}> View caregiver-patients </h3>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>
                                {this.state.isLoadedCP && <CaregiverPatientTable tableData={this.state.tableData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />}
                            </Col>
                        </Row>

                        <br/>

                        <br/>
                        <h3 style={{marginLeft: 60}}> View medication </h3>
                        <br/>
                        <Row>
                            <Col sm={{size: '8', offset: 1}}>
                                {this.state.isLoadedM && <MedicationTable tableData={this.state.tableData}/>}
                                {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                    errorStatus={this.state.errorStatus}
                                    error={this.state.error}
                                />}
                            </Col>
                        </Row>

                    </Card>

                    <Modal isOpen={this.state.selectedAP} toggle={this.toggleFormAddP}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormAddP}> Add Patient: </ModalHeader>
                        <ModalBody>
                            <PatientAddForm reloadHandler={this.reloadP}/>
                        </ModalBody>
                    </Modal>

                    <Modal isOpen={this.state.selectedAC} toggle={this.toggleFormAddC}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormAddC}> Add Caregiver: </ModalHeader>
                        <ModalBody>
                            <CaregiverAddForm reloadHandler={this.reloadC}/>
                        </ModalBody>
                    </Modal>

                    <Modal isOpen={this.state.selectedDU} toggle={this.toggleFormDelete}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormDelete}> Delete user: </ModalHeader>
                        <ModalBody>
                            <UserDeleteForm reloadHandler={this.reload}/>
                        </ModalBody>
                    </Modal>

                    <Modal isOpen={this.state.selectedUP} toggle={this.toggleFormUpdateP}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormUpdateP}> Update patient: </ModalHeader>
                        <ModalBody>
                            <PatientUpdateForm reloadHandler={this.reloadUP}/>
                        </ModalBody>
                    </Modal>

                    <Modal isOpen={this.state.selectedUC} toggle={this.toggleFormUpdateC}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormUpdateC}> Update caregiver: </ModalHeader>
                        <ModalBody>
                            <CaregiverUpdateForm reloadHandler={this.reloadUC}/>
                        </ModalBody>
                    </Modal>

                    <Modal isOpen={this.state.selectedAM} toggle={this.toggleFormAddM}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormAddM}> Add medication: </ModalHeader>
                        <ModalBody>
                            <MedicationAddForm reloadHandler={this.reloadAM}/>
                        </ModalBody>
                    </Modal>


                    <Modal isOpen={this.state.selectedM} toggle={this.toggleFormDeleteM}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormDeleteM}> Delete medication: </ModalHeader>
                        <ModalBody>
                            <MedicationDeleteForm reloadHandler={this.reloadM}/>
                        </ModalBody>
                    </Modal>

                    <Modal isOpen={this.state.selectedUM} toggle={this.toggleFormUpdateM}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormUpdateM}> Update medication: </ModalHeader>
                        <ModalBody>
                            <MedicationUpdateForm reloadHandler={this.reloadUM}/>
                        </ModalBody>
                    </Modal>

                    <Modal isOpen={this.state.selectedUMR} toggle={this.toggleFormUpdateUMR}
                           className={this.props.className} size="lg">
                        <ModalHeader toggle={this.toggleFormUpdateUMR}> Update medical record: </ModalHeader>
                        <ModalBody>
                            <PatientUpdateMedicalRecForm reloadHandler={this.reloadUMR}/>
                        </ModalBody>
                    </Modal>

                </div>
            )
        }else {
            return (
                <div>
                    <h1> You do not have access to this page </h1>
                </div>
            )
        }


    }

}


export default withRouter(DoctorContainer);