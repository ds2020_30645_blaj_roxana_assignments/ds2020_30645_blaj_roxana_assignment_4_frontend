import React from "react";
import Table from "../../commons/tables/table";

const columns = [
    {
        Header: 'CaregiverName',
        accessor: 'caregiverName',
    },
    {
        Header: 'PatientName',
        accessor: 'patientName',
    },

];

const filters = [
    {
        accessor: 'caregiverName',
    }
];

class CaregiverPatientTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                search={filters}
                pageSize={5}
            />
        )
    }
}

export default CaregiverPatientTable;