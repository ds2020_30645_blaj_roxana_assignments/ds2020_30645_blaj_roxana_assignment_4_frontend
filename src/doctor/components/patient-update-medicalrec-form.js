import React from "react";

import{
    Col,
    Row,
    Input,
    Label,
    FormGroup,
    Button
} from 'reactstrap';

import validate from "../validators/patient-validators";
import * as API_USERS from "../api/doctor-api";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";

class PatientUpdateMedicalRecForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                name:{
                    value: '',
                    placeholder:'Change medical record for pacient ',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                medicalRecord:{
                    value: '',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                }}
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            console.log("1 "+updatedControls[updatedFormElementName].valid );
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;

            console.log("Valid form "  + updatedFormElementName+" "+formIsValid);
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    updateMedicalRecord(name, medicalRecord) {
        return API_USERS.updateMedicalRecord(name, medicalRecord, (result, status, error) => {

            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully update medicalRecord "  );
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    handleSubmit() {
        let name = this.state.formControls.name.value;
        let medicalRecord = {
            medicalRecord: this.state.formControls.medicalRecord.value
        }
        this.updateMedicalRecord(name, medicalRecord);
    }

    render() {
        return (
            <div>
                <FormGroup id='name'>
                    <Label for='nameField'> Pacient name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='medicalRecord'>
                    <Label for='medicalRecordField'> Medical Record: </Label>
                    <Input name='medicalRecord' id='medicalRecordField'
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.medicalRecord.value}
                           touched={this.state.formControls.medicalRecord.touched? 1 : 0}
                           valid={this.state.formControls.medicalRecord.valid}
                           required
                    />
                </FormGroup>

                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"}  disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Update medical record </Button>
                    </Col>
                </Row>
                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }

            </div>
        ) ;
    }
}

export default PatientUpdateMedicalRecForm;