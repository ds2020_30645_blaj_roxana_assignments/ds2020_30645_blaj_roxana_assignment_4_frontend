import React from 'react';
import{
    Col,
    Row,
    Input,
    Label,
    FormGroup,
    Button
} from 'reactstrap';

import * as API_USERS from "../../doctor/api/doctor-api";
import validate from "../validators/patient-validators";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class PatientUpdateForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {


                name: {
                    value: '',
                    placeholder: 'Patient name',
                    valid: false,
                    touched: false,
                    validationRules: {

                        isRequired: true
                    }
                },
                newname: {
                    value: '',
                    placeholder: 'Patient new name',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                gender:{
                    value: '',
                    placeholder: 'Patient gender(MALE/FEMALE)',
                    valid: false,
                    touched: false,
                    validationRules: {
                        genderValid: true,
                        isRequired: true
                    }
                },
                address: {
                    value: '',
                    placeholder: 'Cluj, Zorilor, Str. Lalelelor 21...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },

            },
            role:{
                value:'PATIENT'
            },
            medicalRecord:{
                value:'record'
            },
            birthdate:{
                value:'1900-09-09'
            }

        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    updatePatient(name, pacient) {
        return API_USERS.updatePatient(name, pacient, (result, status, error) => {

            console.log(result+"result");
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted patient with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    handleSubmit() {

        let name = this.state.formControls.name.value;

        let patient = {
            name: this.state.formControls.newname.value,
            gender: this.state.formControls.gender.value,
            birthdate: this.state.birthdate.value,
            address: this.state.formControls.address.value,
            medicalRecord: this.state.medicalRecord.value
        };

        console.log(name);
        console.log(patient);
        this.updatePatient(name, patient);
    }

    render() {
        return (
            <div>
                <FormGroup id='name'>
                    <Label for='nameField'> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />
                </FormGroup>

                <FormGroup id='newname'>
                    <Label for='newnameField'> NewName: </Label>
                    <Input name='newname' id='newnameField' placeholder={this.state.formControls.newname.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.newname.value}
                           touched={this.state.formControls.newname.touched? 1 : 0}
                           valid={this.state.formControls.newname.valid}
                           required
                    />
                </FormGroup>


                <FormGroup id='gender'>
                    <Label for='genderField'> Gender: </Label>
                    <Input name='gender' id='genderField' placeholder={this.state.formControls.gender.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.gender.value}
                           touched={this.state.formControls.gender.touched? 1 : 0}
                           valid={this.state.formControls.gender.valid}
                           required
                    />
                    {this.state.formControls.gender.touched && !this.state.formControls.gender.valid &&
                    <div className={"error-message row"}> * Only MALE, FEMALE, NA </div> }

                </FormGroup>

                <FormGroup id='address'>
                    <Label for='addressField'> Address: </Label>
                    <Input name='address' id='addressField' placeholder={this.state.formControls.address.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.address.value}
                           touched={this.state.formControls.address.touched? 1 : 0}
                           valid={this.state.formControls.address.valid}
                           required
                    />

                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"}  disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Update </Button>
                    </Col>
                </Row>
                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }

            </div>
        ) ;
    }
}

export default PatientUpdateForm;