import React from 'react';
import{
    Col,
    Row,
    Input,
    Label,
    FormGroup,
    Button
} from 'reactstrap';

import * as API_USERS from "../../doctor/api/doctor-api";
import validate from "../validators/patient-validators";
import APIResponseErrorMessage from "../../commons/errorhandling/api-response-error-message";


class MedicationUpdateForm extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reloadHandler = this.props.reloadHandler;

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {

                name: {
                    value: '',
                    placeholder: 'Medication name',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                sideEffects: {
                    value: '',
                    placeholder:'sideEffect1, sideEffect2, ...',
                    valid: false,
                    touched: false,
                    validationRules: {
                        isRequired: true
                    }
                },
                dosage:{
                    value: '',
                    placeholder: "0mg",
                    valid: false,
                    touched: false,
                    validationRules: {
                        dossageLimit: true,
                        isRequired: true
                    }
                }
            },


        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }


    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    updateMedication(name, medication) {
        return API_USERS.updateMedication(name, medication, (result, status, error) => {

            console.log(result+"result");
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully inserted medication with id: " + result);
                this.reloadHandler();
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }


    handleSubmit() {
        let medication = {
            name: this.state.formControls.name.value,
            sideEffects: this.state.formControls.sideEffects.value,
            dosage: this.state.formControls.dosage.value,
        };

        let name = this.state.formControls.name.value;

        console.log(name);
        console.log(medication);
        this.updateMedication(name, medication);
    }

    render() {
        return (
            <div>
                <FormGroup id='name'>
                    <Label for='nameField'> Update medication --> Name: </Label>
                    <Input name='name' id='nameField' placeholder={this.state.formControls.name.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.name.value}
                           touched={this.state.formControls.name.touched? 1 : 0}
                           valid={this.state.formControls.name.valid}
                           required
                    />

                </FormGroup>

                <FormGroup id='sideEffects'>
                    <Label for='sideEffectsField'> SideEffects: </Label>
                    <Input name='sideEffects' id='sideEffectsField' placeholder={this.state.formControls.sideEffects.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.sideEffects.value}
                           touched={this.state.formControls.sideEffects.touched? 1 : 0}
                           valid={this.state.formControls.sideEffects.valid}
                           required
                    />

                </FormGroup>

                <FormGroup id='dosage'>
                    <Label for='dosageField'> Dosage: </Label>
                    <Input name='dosage' id='dosageField' placeholder={this.state.formControls.dosage.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.dosage.value}
                           touched={this.state.formControls.dosage.touched? 1 : 0}
                           valid={this.state.formControls.dosage.valid}
                           required
                    />
                    {this.state.formControls.dosage.touched && !this.state.formControls.dosage.valid &&
                    <div className={"error-message row"}> * Must be less than 700mg </div> }
                </FormGroup>


                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"}  disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Update </Button>
                    </Col>
                </Row>
                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }

            </div>
        ) ;
    }
}

export default MedicationUpdateForm;