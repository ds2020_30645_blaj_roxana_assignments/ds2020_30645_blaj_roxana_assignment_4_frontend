import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Button, Container, DropdownItem, Jumbotron, NavLink} from 'reactstrap';
import { withRouter } from "react-router-dom";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white', };

class Home extends React.Component {


    render() {

        return (

            <div>

                    <button type="button"  className="btn  bg-primary float-left ml-5 mt-2 "
                            onClick={() => this.props.history.push("/login")}>
                        Login
                    </button>

                <Jumbotron fluid style={backgroundStyle}>

                </Jumbotron>

            </div>
        )
    };
}

export default withRouter(Home);
