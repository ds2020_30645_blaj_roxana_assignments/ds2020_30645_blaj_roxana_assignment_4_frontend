function performRequestL(request, callback){
    fetch(request)
        .then(
            function(response) {
                if (response.ok) {
                    response.text().then(json => callback(json, response.status,null));
                }
                else {
                    response.text().then(err => callback(null, response.status,  err));
                }
            })
        .catch(function (err) {
            //catch any other unexpected error, and set custom code for error = 1
            callback(null, 1, err)
        });
}

function performRequest(request, callback){
    fetch(request)
        .then(
            function(response) {
                if (response.ok) {
                    response.json().then(json => callback(json, response.status,null));
                }
                else {
                    response.json().then(err => callback(null, response.status,  err));
                }
            })
        .catch(function (err) {
            //catch any other unexpected error, and set custom code for error = 1
            callback(null, 1, err)
        });
}


function performRequestLog(request, callback){
    fetch(request)
        .then(
            function(response) {
                localStorage.clear()
                if (response.ok) {
                    console.log(response.headers.get('Authorization'));
                    response.headers.get('Authorization')
                    localStorage.setItem('token',  response.headers.get('Authorization'));
                    response.then(callback(null, response.status,null));
                }
                else {
                    response.json().then(err => callback(null, response.status,  err));
                }
            })
        .catch(function (err) {
            //catch any other unexpected error, and set custom code for error = 1
            callback(null, 1, err)
        });
}

module.exports = {
    performRequestL,
    performRequest,
    performRequestLog
};
