import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import NavigationBar from './navigation-bar'

import Home from './home/home';
import ErrorPage from './commons/errorhandling/error-page';
import LoginContainer from "./login/login-container";
import DoctorContainer from "./doctor/doctor-container";
import CaregiverContainer from "./caregiver/caregiver-container";
import PatientContainer from "./patient/patient-container";

class App extends React.Component {

    render() {

        return (
            <div>
            <Router>
                <div>
                    <NavigationBar/>
                    <Switch>

                        <Route
                            exact
                            path='/'
                            render={() => <Home/>}
                        />


                        {/*Error*/}
                        <Route
                            exact
                            path='/error'
                            render={() => <ErrorPage/>}
                        />

                    </Switch>

                    <Route
                        exact
                        path='/login'
                        render={() => <LoginContainer/>}
                    />

                    <Route
                        exact
                        path='/doctor'
                        render={() => <DoctorContainer/>}
                    />

                    <Route
                        exact
                        path='/patient'
                        render={() => <PatientContainer/>}
                    />

                    <Route
                        exact
                        path='/caregiver'
                        render={() => <CaregiverContainer/>}
                    />

                </div>

            </Router>
            </div>
        )
    };
}

export default App
