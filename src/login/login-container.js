import React, { useState } from "react";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {
    Button, FormGroup, Input, Label, Col, Row, Jumbotron
} from 'reactstrap';
import * as API_USERS_login from "../login/api/login";
import validate from "./validators/login-validators";
import BackgroundImg from "../commons/images/future-medicine.jpg";
import { withRouter } from "react-router-dom";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "920px",
    backgroundImage: `url(${BackgroundImg})`
};

class LoginContainer extends React.Component {

    constructor(props){
        super(props);

        //this.toggleForm = this.toggleForm.bind(this);

        this.state = {

            errorStatus: 0,
            error: null,

            formIsValid: false,

            formControls: {
                username: {
                    value: '',
                    placeholder: 'Type your username',
                    valid: false,
                    touched: false,
                    validationRules: {
                        minLength: 6,
                        isRequired: true
                    }
                },
                password: {
                    value: '',
                    placeholder: '********',
                    valid: false,
                    touched: false,

                }

            }
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }


    registerUser(user) {

            API_USERS_login.login(user, (result, status, error) => {

                if ( status === 200 ){
                    console.log("Successfully login person with role: " );
                    console.log(localStorage.getItem('token'));
                    console.log(user.username);
                    API_USERS_login.postRole(user.username, (result, status, error) => {
                        if (result !== null && (status === 200)) {
                            console.log(result);
                            localStorage.setItem('usernameU', user.username);
                            localStorage.setItem('role', result);
                            if(result === "DOCTOR"){
                                this.props.history.push("/doctor");
                            }else if(result === "CAREGIVER"){
                                this.props.history.push("/caregiver");

                            }else if(result === "PATIENT"){
                                this.props.history.push("/patient");
                            }
                        }
                    })
                }

            })



        /*return API_USERS_login.postRole(user, (result, status, error) => {
            const usernameU  = this.state;
            const role = this.state;
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully login person with role: " + result.role );

                localStorage.setItem('usernameU', result.username);
                localStorage.setItem('role', result.role);

                if(result.role === "DOCTOR"){
                         this.props.history.push("/doctor");
                }else if(result.role === "CAREGIVER"){
                        this.props.history.push("/caregiver");

                }else if(result.role === "PATIENT"){
                    this.props.history.push("/patient");
                }

            } else {
                localStorage.setItem('usernameU', ''  );
                localStorage.setItem('role','');
                this.setState(({
                        errorStatus: status,
                        error: error

                }));

            }
        });*/
    }


    handleSubmit() {
        let user = {
            username: this.state.formControls.username.value,
            password: this.state.formControls.password.value,
        };

       // console.log(user);
        localStorage.clear();
        this.registerUser(user);
      //  this.loginWithRole();
    }

    handleChange = event => {

        const name = event.target.name;
        const value = event.target.value;

        const updatedControls = this.state.formControls;

        const updatedFormElement = updatedControls[name];

        updatedFormElement.value = value;
        updatedFormElement.touched = true;
        updatedFormElement.valid = validate(value, updatedFormElement.validationRules);
        updatedControls[name] = updatedFormElement;

        let formIsValid = true;
        for (let updatedFormElementName in updatedControls) {
            formIsValid = updatedControls[updatedFormElementName].valid && formIsValid;
        }

        this.setState({
            formControls: updatedControls,
            formIsValid: formIsValid
        });

    };

    render() {


        return (
            <Jumbotron fluid style={backgroundStyle}>
            <div className="container p-4 my-5 bg-light border " >

                <FormGroup id='username'>
                    <Label for='usernameField'> Username: </Label>
                    <Input name='username' id='usernameField' placeholder={this.state.formControls.username.placeholder}
                           onChange={this.handleChange}
                           defaultValue={this.state.formControls.username.value}
                           touched={this.state.formControls.username.touched? 1 : 0}
                           valid={this.state.formControls.username.valid}
                           required
                    />
                    {this.state.formControls.username.touched && !this.state.formControls.username.valid &&
                    <div className={"error-message row"}> * Name must have at least 6 characters </div>}
                </FormGroup>

                <FormGroup id='password'>
                    <Label for='passwordField'> Password: </Label>
                    <Input name='password' id='passwordField' placeholder={this.state.formControls.password.placeholder}
                           onChange={this.handleChange}
                           type="password"
                           defaultValue={this.state.formControls.password.value}
                           touched={this.state.formControls.password.touched? 1 : 0}
                           required
                    />

                </FormGroup>



                <Row>
                    <Col sm={{size: '4', offset: 8}}>
                        <Button type={"submit"} disabled={!this.state.formIsValid} onClick={this.handleSubmit}>  Login </Button>
                    </Col>
                </Row>

                {
                    this.state.errorStatus > 0 &&
                    <APIResponseErrorMessage errorStatus={this.state.errorStatus} error={this.state.error}/>
                }
            </div>
            </Jumbotron>

        );
    }
}

export default withRouter(LoginContainer);