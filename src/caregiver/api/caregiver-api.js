import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    users: '/userr',
    caregivers: '/caregiver/',
    caregiver:'/caregiver',
    name: '/name/',
    cp:"/caregiverPatients"
};

function getCaregiverName(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.users + endpoint.caregivers + username, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequestL(request, callback);
}

function getCaregiverData(name, callback){

    let request = new Request(HOST.backend_api + endpoint.caregiver + endpoint.name + name, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatients(name, callback){
    let request = new Request(HOST.backend_api + endpoint.users + endpoint.cp + endpoint.name + name, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

export{
    getCaregiverName,
    getCaregiverData,
    getPatients
}