
import React from 'react';
import { withRouter } from "react-router-dom";
import { Col, Row} from "reactstrap";
import * as API_USERS from "../caregiver/api/caregiver-api";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import PatientForCaregivTable from "./component/caregiv-patient-table";

class CaregiverContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            nameData:'',
            caregiver:{
                name:'',
                birthdate:'',
                gender:'',
                address:''
            },
            tableData: [],
            isLoaded: false,
            isLoadedCP: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchCaregiverName();

    }

    fetchCaregiverName() {
        const username = localStorage.getItem("usernameU");
        console.log(username);
        return API_USERS.getCaregiverName(username, (result, status, err) => {

            if (result !== null && status === 200) {

                this.setState({
                    nameData: result,
                    isLoaded: true
                });

            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }

            this.fetchCaregiverData();
            this.fetchPatients();

        });

    }

    fetchCaregiverData() {
        let nameC= this.state.nameData;

        return API_USERS.getCaregiverData(nameC, (result, status, err) => {

            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    caregiver: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }

        });
    }

    fetchPatients(){
        let nameC= this.state.nameData;
        return API_USERS.getPatients(nameC, (result, status, err) => {

            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    tableData: result,
                    isLoadedCP: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }

        });
    }


    render() {
        const handleLogout = () => {
            localStorage.clear();
            this.props.history.push("/login")
        };


        const loggedInAsCaregiver =localStorage.getItem("role");

        if (loggedInAsCaregiver==="CAREGIVER") {
            return (

                <div>
                    <div className="bg-light clearfix">

                        <button type="button"  className="btn  bg-secondary float-right mr-2 mt-1"
                                onClick={handleLogout}>
                            Logout
                        </button>
                        <span><h2> Caregiver </h2></span>
                    </div>

                    <br/>
                    <h3 className="ml-3"> Caregiver details:</h3>
                    <p className="ml-5"> Name -->  {this.state.nameData} </p>
                    <p className="ml-5"> Birthdate --> {this.state.caregiver.birthdate} </p>
                    <p className="ml-5"> Gender --> {this.state.caregiver.gender} </p>
                    <p className="ml-5"> Address --> {this.state.caregiver.address} </p>

                    <br/>
                    <h3 style={{marginLeft: 60}}> View patients </h3>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoadedCP && <PatientForCaregivTable tableData={this.state.tableData}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>

                    <br/><br/>


                </div>




            );
        }else{
            return(
                <div>
                    <h1> You do not have access to this page </h1>
                </div>
            );
        }
    }
}

export default withRouter(CaregiverContainer);