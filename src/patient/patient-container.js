import React from 'react';
import { withRouter } from "react-router-dom";
import * as API_USERS from "../patient/api/patient-api";

class PatientContainer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            nameData:'',
            patient:{
                name:'',
                birthdate:'',
                gender:'',
                address:'',
                medicalRecord:'',
                caregiver:''
            },
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchPatientName();

    }

    fetchPatientName(){
        const username = localStorage.getItem("usernameU");
        console.log(username);
        return API_USERS.getPatient(username, (result, status, err) => {

            if (result !== null && status === 200 ) {

                this.setState({
                    nameData:result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
            this.fetchPatientData();
        });

    }

    fetchPatientData() {
        let nameP= this.state.nameData;

        return API_USERS.getPatientData(nameP, (result, status, err) => {

            if (result !== null && status === 200) {
                console.log(result);
                this.setState({
                    patient: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }

        });
    }

    render() {
        const handleLogout = () => {

            localStorage.clear();
            this.props.history.push("/login")
        };

        const loggedInAsPatient =localStorage.getItem("role");

        if (loggedInAsPatient==="PATIENT") {
            return (

                <div>
                    <div className="bg-light clearfix">
                        <button type="button"  className="btn  bg-secondary float-right mr-2 mt-1"
                                onClick={handleLogout}>
                            Logout
                        </button>
                        <span><h2> Patient </h2></span>
                    </div>

                    <br/>
                    <h3 className="ml-3"> Patient details:</h3>
                    <p className="ml-5"> Name -->  {this.state.nameData}</p>
                    <p className="ml-5"> Birthdate --> {this.state.patient.birthdate} </p>
                    <p className="ml-5"> Gender --> {this.state.patient.gender} </p>
                    <p className="ml-5"> Address --> {this.state.patient.address} </p>
                    <p className="ml-5"> Medical record --> {this.state.patient.medicalRecord} </p>
                </div>
            )
        }else{
            return(
                <div>
                    <h1> You do not have access to this page </h1>
                </div>
            );
        }
    }
}

export default withRouter(PatientContainer);