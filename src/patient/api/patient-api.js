import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    users: '/userr',
    patient: '/patient/',
    name:"/name/"
};

function getPatient(username, callback) {
    let request = new Request(HOST.backend_api + endpoint.users + endpoint.patient + username, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequestL(request, callback);
}

function getPatientData(name, callback){
    let request = new Request(HOST.backend_api + endpoint.patient + endpoint.name + name, {
        method: 'GET',
        headers : {
            'Authorization': localStorage.getItem('token'),
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


export{
    getPatient,
    getPatientData
}